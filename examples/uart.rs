#![no_std]
#![no_main]

use cortex_m::asm::delay;
use cortex_m_rt::entry;
use panic_rtt_target as _;
use rtt_target::{rprint, rprintln, rtt_init_print};
use stm32_simple_hal::traits::*;

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("Hello World");
    let p = stm32_simple_hal::stm32::Peripherals::take().unwrap();
    let clocks = p.RCC.take().sysclk(36.mhz()).freeze();

    let gpioa = p.GPIOA.take();
    let gpiob = p.GPIOB.take();
    let gpioc = p.GPIOC.take();
    let gpiod = p.GPIOD.take();

    let tx_pin = gpiob.p6.as_alternate_push_pull_output();
    let rx_pin = gpiob.p7.as_alternate_push_pull_output();

    let serial1 = p
        .USART1
        .uart((tx_pin, rx_pin))
        .baudrate(9600.baud())
        .parity_bits(false);
    let (mut tx, mut _rx) = serial1.split(&clocks);
    tx.send_byte(42);

    let tx_pin2 = gpioa.p2.as_alternate_push_pull_output();
    let rx_pin2 = gpioa.p3.as_alternate_push_pull_output();
    let (mut tx2, rx2) = p.USART2.uart((tx_pin2, rx_pin2)).baudrate(9600.baud()).split(&clocks);

    let tx_pin3 = gpiob.p10.as_alternate_push_pull_output();
    let rx_pin3 = gpiob.p11.as_alternate_push_pull_output();
    let (mut tx3, rx3) = p.USART3.uart((tx_pin3, rx_pin3)).baudrate(9600.baud()).split(&clocks);

    let tx_pin4 = gpioc.p10.as_alternate_push_pull_output();
    let rx_pin4 = gpioc.p11.as_alternate_push_pull_output();
    let (mut tx4, rx4) = p.UART4.uart((tx_pin4, rx_pin4)).baudrate(9600.baud()).split(&clocks);

    let tx_pin5 = gpioc.p12.as_alternate_push_pull_output();
    let rx_pin5 = gpiod.p2.as_alternate_push_pull_output();
    let (mut tx5, rx5) = p.UART5.uart((tx_pin5, rx_pin5)).baudrate(9600.baud()).split(&clocks);

    loop {
        delay(2000000);
        rprint!(".");
        tx.send_byte('a' as u8);
        tx2.send_byte('b' as u8);
        tx3.send_byte('c' as u8);
        tx4.send_byte('d' as u8);
        tx5.send_byte('e' as u8);
    }
}
