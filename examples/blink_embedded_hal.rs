#![no_std]
#![no_main]

use cortex_m::asm::delay;
use cortex_m_rt::entry;
use embedded_hal::digital::v2::*;
use panic_rtt_target as _;
use rtt_target::{rprint, rprintln, rtt_init_print};
use stm32_simple_hal::traits::*;

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("Hello World");

    let p = stm32_simple_hal::stm32::Peripherals::take().unwrap();

    let gpioc = p.GPIOC.take();
    let mut pc13 = gpioc.p13.as_push_pull_output();
    loop {
        delay(2000000);
        set_emb_hal_pin(&mut pc13);
        delay(2000000);
        pc13.set_low();
        rprint!(".");
    }
}

/// An example function which uses the embedded_hal interface to show that our hal provides that
/// too.
fn set_emb_hal_pin<PIN: OutputPin>(p: &mut PIN)
where
    <PIN as OutputPin>::Error: core::fmt::Debug,
{
    p.set_high().unwrap();
}
