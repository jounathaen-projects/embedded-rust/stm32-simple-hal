#![no_std]
#![no_main]

use cortex_m::asm::delay;
use cortex_m_rt::entry;
use panic_rtt_target as _;
use rtt_target::{rprint, rprintln, rtt_init_print};
use stm32_simple_hal::traits::*;

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("Hello World");
    let p = stm32_simple_hal::stm32::Peripherals::take().unwrap();

    let gpioa = p.GPIOA.take();
    let gpiob = p.GPIOB.take();
    let gpioc = p.GPIOC.take();
    let pa1 = gpioa.p1.as_pull_up_input();

    let mut pb12 = gpiob.p12.as_push_pull_output();
    pb12.toggle();
    let mut pb12 = pb12.as_tri_state_output();
    pb12.set_high();
    pb12.set_low();
    pb12.set_high_z();

    let mut led = gpioc.p13.as_push_pull_output();
    loop {
        delay(2000000);
        led.set_high();
        delay(2000000);
        led.set_low();
        if pa1.is_high() {
            rprint!("|");
        } else {
            rprint!(".");
        }
    }
}
