use crate::gpio::{pinstates::AlternateFunction, Pin};
use crate::rcc::Clocks;
use crate::stm32::{AFIO, UART4, UART5, USART1, USART2, USART3};
use crate::time::{Baud, Hertz, U32Ext};
use core::marker::PhantomData;

mod internals {
    //! Sealed trait trick to ensure `UartFns` can't be called and implemented outside this crate
    use crate::rcc::Clocks;
    use crate::stm32::uart4::RegisterBlock;
    use crate::stm32::{AFIO, RCC, UART4, UART5, USART1, USART2, USART3};
    use crate::time::Hertz;

    pub trait UartFns {
        //type Usart;
        fn enable_register_clk();
        fn get_register_block() -> *const RegisterBlock;
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz;
        fn ensure_pin_mapping(&mut self, remap_bits: u8);
    }
    impl UartFns for crate::stm32::USART1 {
        fn enable_register_clk() {
            // Safety: Atomic write which doesn't has side effects.
            unsafe { (*RCC::ptr()).apb2enr.modify(|_, w| w.usart1en().set_bit()) }
        }
        fn get_register_block() -> *const RegisterBlock {
            USART1::ptr() as *const RegisterBlock
        }
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz {
            clocks.pclk2()
        }
        fn ensure_pin_mapping(&mut self, remap_bits: u8) {
            // Safety: we only atomically modify the bits for the usart1 we have ownership for.
            // This function should only be called when a pin was marked alternate which enables
            // the afio clock.
            unsafe {
                (*AFIO::ptr())
                    .mapr
                    .modify(|_, w| w.usart1_remap().bit(remap_bits == 1))
            }
        }
    }

    impl UartFns for crate::stm32::USART2 {
        fn enable_register_clk() {
            // Safety: Atomic write which doesn't has side effects.
            unsafe { (*RCC::ptr()).apb1enr.modify(|_, w| w.usart2en().set_bit()) }
        }
        fn get_register_block() -> *const RegisterBlock {
            USART2::ptr() as *const RegisterBlock
        }
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz {
            clocks.pclk1()
        }
        fn ensure_pin_mapping(&mut self, remap_bits: u8) {
            // Safety: we only atomically modify the bits for the usart1 we have ownership for.
            // This function should only be called when a pin was marked alternate which enables
            // the afio clock.
            unsafe {
                (*AFIO::ptr())
                    .mapr
                    .modify(|_, w| w.usart2_remap().bit(remap_bits == 1))
            }
        }
    }

    impl UartFns for crate::stm32::USART3 {
        fn enable_register_clk() {
            // Safety: Atomic write which doesn't has side effects.
            unsafe { (*RCC::ptr()).apb1enr.modify(|_, w| w.usart3en().set_bit()) }
        }
        fn get_register_block() -> *const RegisterBlock {
            USART3::ptr() as *const RegisterBlock
        }
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz {
            clocks.pclk1()
        }
        fn ensure_pin_mapping(&mut self, remap_bits: u8) {
            // Safety: we only atomically modify the bits for the usart1 we have ownership for.
            // This function should only be called when a pin was marked alternate which enables
            // the afio clock.
            unsafe {
                (*AFIO::ptr())
                    .mapr
                    .modify(|_, w| w.usart3_remap().bits(remap_bits))
            }
        }
    }

    impl UartFns for crate::stm32::UART4 {
        fn enable_register_clk() {
            // Safety: Atomic write which doesn't has side effects.
            unsafe { (*RCC::ptr()).apb1enr.modify(|_, w| w.uart4en().set_bit()) }
        }
        fn get_register_block() -> *const RegisterBlock {
            UART4::ptr()
        }
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz {
            clocks.pclk1()
        }
        fn ensure_pin_mapping(&mut self, _remap_bits: u8) {
            unreachable!("No remap for UART4")
        }
    }

    impl UartFns for crate::stm32::UART5 {
        fn enable_register_clk() {
            // Safety: Atomic write which doesn't has side effects.
            unsafe { (*RCC::ptr()).apb1enr.modify(|_, w| w.uart5en().set_bit()) }
        }
        fn get_register_block() -> *const RegisterBlock {
            UART5::ptr()
        }
        fn get_input_clock_frequency(clocks: &Clocks) -> Hertz {
            clocks.pclk1()
        }
        fn ensure_pin_mapping(&mut self, _remap_bits: u8) {
            unreachable!("No remap for UART5")
        }
    }
}

/// This pin marks pin pairs that can be used for tx and rx for the uart `UART`.
pub trait UartPins<UART> {
    fn get_remap_bits() -> Option<u8>;
}

macro_rules! impl_uart_pins {
    ($serial:ident, $bank1:literal , $pin1:literal , $bank2:literal , $pin2:literal, $bits:expr ) => {
        impl<MODE> UartPins<$serial>
            for (
                Pin<AlternateFunction<MODE>, $bank1, $pin1>,
                Pin<AlternateFunction<MODE>, $bank2, $pin2>,
            )
        {
            fn get_remap_bits() -> Option<u8> {
                $bits
            }
        }
    };
}
impl_uart_pins! {USART1, 'A', 9, 'A', 10, Some(0)}
impl_uart_pins! {USART1, 'B', 6, 'B', 7, Some(1)}

impl_uart_pins! {USART2, 'A', 2, 'A', 3, Some(0)}
impl_uart_pins! {USART2, 'D', 5, 'D', 6, Some(1)} // Remap available only for 100-pin and 144-pin packages.

impl_uart_pins! {USART3, 'B', 10, 'B', 11, Some(0)}
impl_uart_pins! {USART3, 'C', 10, 'C', 11, Some(0b01)} // Remap available only for 64-pin, 100-pin and 144-pin packages
impl_uart_pins! {USART3, 'D', 8, 'D', 9, Some(0b11)} // Remap available only for 100-pin and 144-pin packages.

impl_uart_pins! {UART4, 'C', 10, 'C', 11, None}

impl_uart_pins! {UART5, 'C', 12, 'D', 2, None}

#[derive(Clone, Copy, Debug)]
pub enum StopBits {
    Bits1 = 0,
    Bits0_5 = 0b01,
    Bits2 = 0b10,
    Bits1_5 = 0b11,
}

/// Intermediate construction struct for serial communication. Can be created with the
/// [`UartAccess::serial`] function.
///
/// Implements the [builder pattern](https://doc.rust-lang.org/1.0.0/style/ownership/builders.html)
/// to comfortably configure the serial peripheral.
pub struct Uart<SERIAL> {
    _usart_marker: PhantomData<SERIAL>,
    baudrate: Baud,
    parity_bits: bool,
    parity_selection_odd: bool,
    stop_bits: StopBits,
    //word_length: bool, //TODO
}
impl<SERIAL: internals::UartFns> Uart<SERIAL> {
    fn new<PINS: UartPins<SERIAL>>(mut usart: SERIAL) -> Self {
        if let Some(remap_bits) = PINS::get_remap_bits() {
            usart.ensure_pin_mapping(remap_bits);
        }
        Self {
            _usart_marker: Default::default(),
            baudrate: 9600.baud(),
            parity_bits: false,
            parity_selection_odd: false,
            stop_bits: StopBits::Bits1,
        }
    }

    pub fn baudrate(mut self, baudrate: Baud) -> Self {
        self.baudrate = baudrate;
        self
    }

    // TODO This also affects the nr of bits
    pub fn parity_bits(mut self, enable: bool) -> Self {
        self.parity_bits = enable;
        self
    }

    // TODO: Join with parity_bits
    pub fn odd_parity_selection(mut self, odd: bool) -> Self {
        self.parity_selection_odd = odd;
        self
    }

    pub fn stop_bits(mut self, stopbits: StopBits) -> Self {
        self.stop_bits = stopbits;
        self
    }

    pub fn split(self, clocks: &Clocks) -> (Tx<SERIAL>, Rx<SERIAL>) {
        SERIAL::enable_register_clk();
        // Safety: We only acess the uart's own registers and the uart is a singleton
        unsafe {
            let uart_regs = &*SERIAL::get_register_block();
            uart_regs.cr1.modify(|_r, w| {
                w.ue().set_bit();
                w.pce().bit(self.parity_bits);
                w.ps().bit(self.parity_selection_odd);
                w.te().set_bit();
                w
            });
            uart_regs.cr2.modify(|_r, w| {
                w.stop().bits(self.stop_bits as u8);
                w
            });
            uart_regs.brr.write(|w| {
                w.bits(calculate_baud_cfg_bits(
                    SERIAL::get_input_clock_frequency(&clocks),
                    self.baudrate,
                ) as u32)
            });
        }
        (Tx::new(), Rx::new())
    }
}

fn calculate_baud_cfg_bits(fclk: Hertz, baud: Baud) -> u16 {
    // The reference manual makes the description of the baudrate configuration harder than
    // necessary:
    // Instead of calculating fclk/(16 * baud) and then dividing it in fraction and mantissa,
    // simply calculate fclk/baud and the lower 4 bits are the fraction. As the bits are arranged
    // that way in the same register, we can simply write that value there.
    (fclk.0 / baud.0) as u16
}

/// Extension trait for the GPIO structs in [`crate::stm32`] to create the `GPIOx` structs.
///
/// Example use:
/// ```
/// let p = stm32_simple_hal::stm32::Peripherals::take().unwrap();
/// let (tx, rx) = p.USART1.serial((rx_pin, tx_pin)).split();
/// ```
pub trait SerialAccess {
    fn uart<PINS: UartPins<Self>>(self, tx_rx_pins: PINS) -> Uart<Self>
    where
        Self: Sized,
        Self: internals::UartFns,
    {
        drop(tx_rx_pins);
        Uart::new::<PINS>(self)
    }
}

impl SerialAccess for crate::stm32::USART1 {}
impl SerialAccess for crate::stm32::USART2 {}
impl SerialAccess for crate::stm32::USART3 {}
impl SerialAccess for crate::stm32::UART4 {}
impl SerialAccess for crate::stm32::UART5 {}

/// Serial receiver
pub struct Rx<SERIAL> {
    _usart: PhantomData<SERIAL>,
}
impl<UART> Rx<UART> {
    fn new() -> Self {
        Self {
            _usart: Default::default(),
        }
    }
}

/// Serial transmitter
pub struct Tx<SERIAL> {
    _usart: PhantomData<SERIAL>,
}
impl<SERIAL: internals::UartFns> Tx<SERIAL> {
    fn new() -> Self {
        Self {
            _usart: Default::default(),
        }
    }

    pub fn send_byte(&mut self, byte: u8) {
        let regs = SERIAL::get_register_block();
        unsafe { (*regs).dr.write(|w| w.bits(byte as u32)) };
    }
}
