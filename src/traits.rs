//! Commodity module to import all the traits extending the [peripherals in `stm32`](crate::stm32)
//! at once.
//!
//! Example:
//! ```
//!  #![no_std]
//!  #![no_main]
//!
//!  use cortex_m_rt::entry;
//!  use stm32_simple_hal::traits::*;
//!
//!  #[entry]
//!  fn main() -> ! {
//!      let p = stm32_simple_hal::stm32::Peripherals::take().unwrap();
//!      let gpioa = p.GPIOA.take(); // This would fail or require the import of
//!                                  // `stm32_simple_hal::gpio::GpioAccess`
//!  }
//! ```

pub use crate::gpio::GpioAccess;
pub use crate::rcc::RccAccess;
pub use crate::serial::SerialAccess;
pub use crate::time::U32Ext;
