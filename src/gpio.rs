//! General Purpose Input & Output
//!
//! Most of this is **heavily** inspired by <https://www.ecorax.net/macro-bunker-1/>

use crate::rcc::enable_afio;
use crate::stm32::{gpioa::RegisterBlock, RCC};
use core::convert::Infallible;
use core::marker::PhantomData;
use embedded_hal::digital::v2::PinState;

pub mod pinstates {
    //! Types and enums to ensure correct pin configuration before use.

    use core::marker::PhantomData;

    pub struct Floating;
    pub struct PullDown;
    pub struct PullUp;

    pub struct PushPull;
    pub struct OpenDrain;

    pub struct NotConfigured;
    pub struct Input<MODE> {
        _marker: PhantomData<MODE>,
    }
    pub struct Output<MODE> {
        _marker: PhantomData<MODE>,
    }
    pub struct TriState;
    pub struct Analog;
    pub struct AlternateFunction<MODE> {
        _marker: PhantomData<MODE>,
    }
}
use pinstates::*;

/// Extension trait for the GPIO structs in [`crate::stm32`] to create the `GPIOx` structs.
pub trait GpioAccess {
    type Gpiobank;
    /// Consume the raw peripheral to get access the gpio bank.
    fn take(self) -> Self::Gpiobank;
}

// -----------------------------------------

/// Defines Gpio structs with 16 pins which looks like:
/// ```rust
/// pub struct GpioC {
///     pub p0: Pin<NotConfigured, 'C', 0>,
///     pub p1: Pin<NotConfigured, 'C', 1>,
///     // ...
/// }
/// impl GpioC {
///     pub fn new() -> Self {
///         unsafe { (*RCC::ptr()).apb2enr.modify(|_, w| w.iopcen().set_bit()) }
///         Self {
///             p0: Pin::new(),
///             p1: Pin::new(),
///             // ...
///         }
///     }
/// }
/// ```
macro_rules! gpio_struct {
    ($character:tt $( $number:tt )*) => { paste::item! {

        /// Singleton struct to give access to the pins
        #[doc = "of GPIO bank " [<$character>] "."]
        ///
        /// This mechanism enables the borrow checker to ensure
        /// that each pin is a singleton and thus no unowned access to the pins
        /// can happen.
        ///
        /// # Example
        /// ```
        #[doc = "let gpio" [<$character:lower>] " = gpio::Gpio" [<$character>] "::new();"]
        #[doc = "let mut pin = gpio" [<$character:lower>] ".p13.as_push_pull_output();"]
        /// pin.set_high()
        /// ```
        pub struct [<Gpio $character>] {
                $(
                    pub [<p $number>]: Pin<NotConfigured, $character, $number>,
                )+
            }
            impl [<Gpio $character>] {
                #[doc = "Constructs a new `"[<Gpio $character>]"` struct"]
                /// which is the only way to get access to the gpio pins.
                #[allow(clippy::new_without_default)]
                fn new() -> Self {
                    unsafe { (*RCC::ptr()).apb2enr.modify(|_, w| w.[<iop $character:lower en>]().set_bit()) }
                    Self {
                        $([<p $number>]: Pin::new(),)*
                    }
                }
            }
            impl  GpioAccess for crate::stm32::[<GPIO $character>] {
                type Gpiobank = [<Gpio $character>];
                fn take(self) -> Self::Gpiobank {
                    [<Gpio $character>] ::new()
                }
            }

        }

    }
}

/// Takes a space separated list of chars and constructs a gpio struct and the `get_port_registers`
/// function for each letter. Must only be called once!
macro_rules! construct_16_pin_gpio_banks {
    ([$($character:literal)*]) => {
        $(
            gpio_struct! {$character 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15}
        )+

        paste::item! {
            fn get_port_registers(c: char) -> *const RegisterBlock{
                match c {
                    $($character => crate::stm32::[<GPIO $character>]::ptr(),)*
                    _ => unimplemented!(),
                }
            }
        }
    };
}

construct_16_pin_gpio_banks! {['A' 'B' 'C' 'D' 'E' 'F' 'G']}

// -----------------------------------------

/// This struct represents a physical GPIO pin.
///
/// Every pin is a singleton and can only be obtained via the corresponding GPIO struct (e.g. via [`GpioA`])
pub struct Pin<MODE, const PORT: char, const INDEX: u8> {
    _marker: PhantomData<MODE>,
}
impl<MODE, const PORT: char, const INDEX: u8> Pin<MODE, PORT, INDEX> {
    // Inner constructor. Only the GPIO module can call it to ensure
    // there only exists one of each pin.
    fn new() -> Self {
        Self {
            _marker: Default::default(),
        }
    }

    /// Sets the pin to _analog_ input mode.
    pub fn as_analog_input(self) -> Pin<Analog, PORT, INDEX> {
        Self::set_configuration_bits(0b00, 0b00);
        Pin::new()
    }

    /// Sets the pin to _floating_ input mode.
    pub fn as_floating_input(self) -> Pin<Input<Floating>, PORT, INDEX> {
        Self::set_configuration_bits(0b01, 0b00);
        Pin::new()
    }

    /// Sets the pin to _pull down_ input mode.
    pub fn as_pull_down_input(self) -> Pin<Input<PullDown>, PORT, INDEX> {
        Self::set_configuration_bits(0b10, 0b00);
        Self::set_odr(0b0);
        Pin::new()
    }

    /// Sets the pin to _pull up_ input mode.
    pub fn as_pull_up_input(self) -> Pin<Input<PullUp>, PORT, INDEX> {
        Self::set_configuration_bits(0b10, 0b00);
        Self::set_odr(0b1);
        Pin::new()
    }

    /// Sets the pin to _push-pull_ output mode.
    pub fn as_push_pull_output(self) -> Pin<Output<PushPull>, PORT, INDEX> {
        Self::set_configuration_bits(0b00, 0b11);
        Pin::new()
    }

    /// Sets the pin to _open drain_ output mode.
    pub fn as_open_drain_output(self) -> Pin<Output<OpenDrain>, PORT, INDEX> {
        Self::set_configuration_bits(0b01, 0b11);
        enable_afio();
        Pin::new()
    }

    /// Put's the pin in _alternate push-pull_ mode
    pub fn as_alternate_push_pull_output(self) -> Pin<AlternateFunction<PushPull>, PORT, INDEX> {
        Self::set_configuration_bits(0b10, 0b11);
        enable_afio();
        Pin::new()
    }

    /// Put's the pin in _alternate open drain_ mode
    pub fn as_alternate_open_drain_output(self) -> Pin<AlternateFunction<OpenDrain>, PORT, INDEX> {
        Self::set_configuration_bits(0b11, 0b11);
        Pin::new()
    }

    /// Sets the pin to push_pull output mode.
    pub fn as_tri_state_output(self) -> Pin<TriState, PORT, INDEX> {
        Self::set_configuration_bits(0b01, 0b11);
        Pin::new()
    }

    /// Disables the pin.
    pub fn as_disabled(self) -> Pin<NotConfigured, PORT, INDEX> {
        Self::set_configuration_bits(0b01, 0b00);
        Pin::new()
    }

    /// Internal commodity function to set the configuration registers accordingly
    fn set_configuration_bits(cnf_bits: u8, mode_bits: u8) {
        let offset = (4 * INDEX) % 32;
        let high_register_threshold = 8u8;
        let bit_cfg = (cnf_bits << 2 | mode_bits) as u32;
        let mask = 0b_1111;

        // Safety: Each pin is a singleton and we only set the bits which affect this very pin
        unsafe {
            if INDEX <= high_register_threshold {
                (*get_port_registers(PORT))
                    .crl
                    .modify(|r, w| w.bits((r.bits() & !(mask << offset)) | (bit_cfg << offset)));
            } else {
                (*get_port_registers(PORT))
                    .crh
                    .modify(|r, w| w.bits((r.bits() & !(mask << offset)) | (bit_cfg << offset)));
            };
        }
    }

    fn set_output(pinstate: PinState) {
        // Safety: Each pin is a singleton and we only set the bits which affect this very pin
        unsafe {
            match pinstate {
                PinState::Low => {
                    (*get_port_registers(PORT))
                        .brr
                        .write(|w| w.bits(0b1 << INDEX));
                }
                PinState::High => {
                    (*get_port_registers(PORT))
                        .bsrr
                        .write(|w| w.bits(0b1 << INDEX));
                }
            }
        }
    }

    fn set_odr(odr_bit: u8) {
        assert!(odr_bit < 2);
        // Safety: Each pin is a singleton and we only set the bits which affect this very pin
        unsafe {
            (*get_port_registers(PORT))
                .odr
                .write(|w| w.bits((odr_bit as u32) << INDEX));
        }
    }

    fn internal_is_high() -> bool {
        // Safety: Each pin is a singleton and we only set the bits which affect this very pin
        unsafe { ((*get_port_registers(PORT)).idr.read().bits() >> INDEX) & 0b1 == 0b1 }
    }

    fn internal_toggle() {
        cortex_m::interrupt::free(|_| {
            if Self::internal_is_high() {
                Self::set_output(PinState::Low);
            } else {
                Self::set_output(PinState::High);
            }
        });
    }
}

/// Functions only available on output pins.
///
/// Output pins can be created via [`Pin::as_open_drain_output`] or
/// [`Pin::as_push_pull_output`]
impl<OUTPUTMODE, const PORT: char, const INDEX: u8> Pin<Output<OUTPUTMODE>, PORT, INDEX> {
    /// Sets an output pin's output to _low_ state.
    pub fn set_low(&mut self) {
        Self::set_output(PinState::Low);
    }

    /// Sets an output pin's output to _high_ state.
    pub fn set_high(&mut self) {
        Self::set_output(PinState::High);
    }

    /// Toggles an output pin's state.
    pub fn toggle(&mut self) {
        Self::internal_toggle()
    }
}
impl<OUTPUTMODE, const PORT: char, const INDEX: u8> embedded_hal::digital::v2::OutputPin
    for Pin<Output<OUTPUTMODE>, PORT, INDEX>
{
    type Error = Infallible;
    fn set_low(&mut self) -> Result<(), Self::Error> {
        Self::set_output(PinState::Low);
        Ok(())
    }
    fn set_high(&mut self) -> Result<(), Self::Error> {
        Self::set_output(PinState::High);
        Ok(())
    }

    fn set_state(&mut self, state: PinState) -> Result<(), Self::Error> {
        Self::set_output(state);
        Ok(())
    }
}

/// Functions only available on input pins.
///
/// Input pins can be created via [`Pin::as_floating_input`],
/// [`Pin::as_pull_up_input`]  or [`Pin::as_pull_down_input`]
impl<INPUTMODE, const PORT: char, const INDEX: u8> Pin<Input<INPUTMODE>, PORT, INDEX> {
    /// Checks if an input pin's state is _high_.
    pub fn is_high(&self) -> bool {
        Self::internal_is_high()
    }

    /// Checks if an input pin's state is _low_.
    pub fn is_low(&self) -> bool {
        !Self::internal_is_high()
    }

    /// Queries the current pin's state.
    pub fn get_state(&self) -> PinState {
        if Self::internal_is_high() {
            PinState::High
        } else {
            PinState::Low
        }
    }
}

/// Functions only available on TriState pins.
///
/// TriState pins can be created via [`Pin::as_tri_state_output`].
impl<const PORT: char, const INDEX: u8> Pin<TriState, PORT, INDEX> {
    /// Sets an output pin's output to _low_ state.
    pub fn set_low(&mut self) {
        cortex_m::interrupt::free(|_| {
            Self::set_configuration_bits(0b10, 0b11);
            cortex_m::asm::nop(); // required to wait until the pin mode has changed
            Self::set_output(PinState::Low);
        });
    }

    /// Sets an output pin's output to _high_ state.
    pub fn set_high(&mut self) {
        Self::set_configuration_bits(0b10, 0b11);
        cortex_m::asm::nop(); // required to wait until the pin mode has changed
        Self::set_output(PinState::High);
    }

    /// Sets an output pin's output to _high-z_ state. This is basically a floating pin.
    pub fn set_high_z(&mut self) {
        Self::set_configuration_bits(0b01, 0b00); // Floating input equals high-z
        cortex_m::asm::nop(); // required to wait until the pin mode has changed
    }

    /// Checks if an input pin's state is _high_.
    pub fn is_high(&self) -> bool {
        Self::internal_is_high()
    }

    /// Checks if an input pin's state is _low_.
    pub fn is_low(&self) -> bool {
        !Self::internal_is_high()
    }

    /// Queries the current pin's state.
    pub fn get_state(&self) -> PinState {
        if Self::internal_is_high() {
            PinState::High
        } else {
            PinState::Low
        }
    }
}
